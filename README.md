minimal nodejs site for faucet

We build the application in two stages. First we crete a docker image
that we upload on dockerhub.

cd app
docker build . -t tezos/faucet
docker login
docker push tezos/faucet

This operation has to be done every time we change something in the application itself.
This image does not contain the recaptcha_keys.json and secret_seeds.json necessary to
run the faucet application.

To run a local instance of the app, we build the final image including
the recaptcha_keys.json secret_seeds.json

mkdir secrets
copy recaptcha_keys.json and secret_seeds.json in the secrets directory then run

docker-compose up --build

This will create and run the final image. You can check the result on
http://localhost:8081

(probably the recatchpa will not work)

To deploy the app on faucet.tzalpha.net go to the alphanet-admin repository and
deploy using the ansible scripts.
